$(function() 
    {
        var bouton = $("#menu li"),
        cube = $("#cube"),
        presentation  = $("div.wrap"),
        contCube = $("#cube").parent(),
        wrap = $("div.wrap"),
        docScroll = $(window).scrollTop(),
        W_height = $(window).height(),
        timeAnim = 1300,
        cubeMargin = Math.ceil(((wrap.outerHeight()/2) - (cube.outerHeight() /2))  + 70) ,
        W_height = $(window).height();
   
       
        var cubePos = $("section.container").position().top;
        var fleche = $("#fleche");
    
		
        function init(){
            console.log(cubeMargin);
            // Initialisation de la taille des 2 div principales en fonction de la taille écran utilisateur
            wrap.css("height", W_height);
        
            // Initialisation de la marge appliqué au cube pour le centrer horizontalement
            contCube.css("marginTop",cubeMargin)
        
            // initialisation de la boucle pour l'animation de la flèche
            animFleche();
            setInterval(animFleche,2000);
        
            // centrage de la flèche verticalement
            fleche.css("left", ($(document).width() / 2) - (fleche.width() / 2)) 
        
            console.log(" wrap /2 : " + (wrap.outerHeight()/2) + "px - " + "Cube /2 : " + (cube.outerHeight() /2) + "px - " + "marge appliqué : " + cubeMargin + "px" );
            
            scrollEvent();
            
        }
		
        function scrollEvent(){
            $(document).on('mousewheel', animScroll);
        }
		
        $(window).resize(function(){
            W_height = $(window).height();
            wrap.css("height", W_height);
            contCube.css("marginTop",cubeMargin);
            cubeTop = $("section.container").position().topf; 
        })
	
//        $(".lien").click(function(){
//            console.log("avant chgmts css div cache");
//            $(".cache").css("display","block");
//            $(".cache").css("position","absolute");
//            $(".cache").css("border","solid red 1px");
//            $(".cache").css("width","170%");
//        });
		
		
		/******** fonction gestion du scroll *********/
		
		//scroll lors d'un click sur la fleche
		$("#fleche").click(function(){
		
			var cubePos = $("section.container").parent().position().top -  100;
			$("html,body").animate({
                scrollTop: cubePos 
            }, 'slow');
		});
		
		//scroll souris
        function animScroll(e,delta){
            
			$(document).off('mousewheel');
            var cubePos = $("section.container").parent().position().top -  100;
			
            if(delta < 0)
            {
                
                $("html,body").animate({
                    scrollTop: cubePos 
                }, 'slow',scrollEvent);
            }  
                
            if(delta > 0)
            {
                $("html,body").animate({
                    scrollTop: 0
                }, 'slow',scrollEvent);
            }
        }
    
		//scroll clavier
        $(window).keydown(function(e){
			
			var cubePos = $("section.container").parent().position().top - 100 ;
            var touche = e.keyCode;
			if(touche == 38){
				console.log("case 38");  
				$('html, body').animate({
                    scrollTop: 0
                }, 'slow');
			}
			else if(touche == 40){
				$('html, body').animate({
                    scrollTop: cubePos
                }, 'slow');
			}
               
        })
		
		//scroll lors d'un click sur un item du menu
		bouton.click(function(){
        
            var cubePos = $("section.container").parent().position().top - 100 ;
			
			//animation lors d'un scroll
            $("html, body").animate({
                scrollTop: cubePos
            }, 'slow');
			
			//rotation du cube pour le changement de face
            var cubeClass = cube.attr("class");
            var currentClass = (this.className);    
            cube.removeClass(cubeClass);
            cube.addClass(currentClass);
            
        });
		
		/******** fin fonctions scroll ********/
		
        function animFleche(){
            fleche.animate({
                bottom: '+50'
            },timeAnim,function(){
                // fonction callback
                fleche.animate({
                    bottom: "20"
                },timeAnim)
            })
        }
		
        
    

        init();
	
    
    
    //    $('#cube').mousedown(function(e){
    //        mouse_pressed =true;
    //        x_init = e.pageX;
    //        y_init = e.pageY;
    //    });
    //    $('body').mousemove(function(e){
    //        if(mouse_pressed)
    //        {
    //					
    //            //alert("mouseup"+x_init+" "+y_init);
    //            x_end = e.pageX;
    //            y_end = e.pageY;
    //					
    //            //alert("mousedown init"+x_init+" "+y_init+"\n"
    //            //+"mouseup end"+x_end+" "+y_end);
    //            delta_x = x_init - x_end;
    //            delta_y = y_init - y_end;
    //					
    //            //var rotateCSS = 'rotate(' + 60 + 'deg)';
    //            if(Math.abs(delta_x) > 0  && Math.abs(delta_y) > 0)
    //            {
    //                var degree_rotation_x = delta_x % 360;
    //                var degree_rotation_y = delta_y % 360;
    //                //alert(degree_rotation);
    //						
    //                $('#cube').css('-webkit-transform','rotateY('+degree_rotation_x+'deg) rotateX('+degree_rotation_y+'deg)');
    //            }
    //        }
    //    });
    //    $('body').mouseup(function(e){
    //        mouse_pressed = false;
    //    });
    });

